from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def generate_GCL(_a, _m, _donnees):
    for n in range(1, _donnees):
        # Calcul de la valeur n de la suite
        res = (_a * GCL[n - 1]) % _m
        GCL.append(res)  # Ajout de la valeur dans

# Données & Initialisation
a =65539
m = 2**31 # Modulo
donnees = 30000 # Nombre de données
x0 = 12345 # x0
GCL = [] # Xn+1 = (65539 * Xn ) [2^31] (RANDU)
GCL.append(x0) # GCL = [x0]

generate_GCL(a,m,donnees)

# # Génération de RANDU
# for n in range(1, donnees):
#     # Calcul de la valeur n de la suite
#     res = (a * GCL[n - 1]) % m
#     GCL.append(res)  # Ajout de la valeur dans

"""
On cherche maintenant une suite Un qui depend de Xn dans l'intervalle [0,1]
"""

# Initialisation de la Suite Un
Un = [] # Un = Xn / [2^31]

# Génération de la suite Un
for element in GCL:
    # Calcul selon la formule
    res = element / m # Un = Xn / [2^31]
    Un.append(res)

"""
=================
TESTS du KI 2
=================
"""

# On va maintenant récuperer les valeurs à partir de l'histogramme
counts, bins, bars = plt.hist(Un, color='#0070FF', edgecolor='#0046FF') # Bins de 10 par défaut
plt.grid(axis='y' , color='black')
plt.xlabel('Valeurs posibles de Un')
plt.ylabel('Effectif')
plt.title('Effectif réel de la suite Un avec '+str(donnees) +' données')
plt.show()
# Hypothese : ca va passer le KHI 2 car peu d'intervalle

# Calcul du Khi
khi2 = 0
theorique = donnees/10 # Valeur théorique de la loi uniforme

# Addition afin d'obtenir la valeur du Khi
for element in counts:
    khi2 += (theorique - element)**2 / element

# On regarde dans le tableau, on a 9 de degres de liberté (10-1), et on prend un sueil de 1%  (khi deux < 21.92
print("Valeur du khi2 = " + str(khi2))
print("On a alors " + str(khi2) + " < " + str(len(bins)) + ". Alors, il y a accord !")

"""
=================
TESTS Spectral 2D
=================
"""

# Test Spectral 2D
plt.plot(GCL[0:len(GCL)-1], GCL[1:], 'b.')
plt.show()
# Aucune structure du coup c'est bon

"""
=================
TESTS Spectral 3D
=================
"""

# Test Spectral 3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Axes
x = GCL[0:len(GCL)-2]
y = GCL[1:len(GCL)-1]
z = GCL[2:]

ax.scatter(x,y,z, c='r' ,marker='o')
plt.title('Test Spectral 3B avec '+str(donnees) +' données')
plt.show()

# Faut tourner la structure pour voir des lignes droites apparaitre